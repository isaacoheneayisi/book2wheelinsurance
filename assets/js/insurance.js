var add_user_form = $("#add_candidates").validate({
	rules: {		
		name: "required",
		contact: "required",
		city: "required",
		bike: "required",	
		yearbike :{
               required: true,
               number: true,
               maxlength: 4,
               minlength: 4
             },
		cost:{
           required : true,
           number: true
         },
    //engine:"required",
    //plate:"required",	
    optradio: "required",
		email: {
			required: true,
			email: true
		}
				
	},
	messages: {
		name: "Please enter name of company",
		contact:"Please enter telephone number for contact person",
		email: "Please enter a valid email address",
    yearbike: "This field contain only 4 values",
    cost: "This field accept only integers"
		
	}
});

$('#add_candidates').submit(function(e){
e.preventDefault();
console.log($("#add_candidates").valid());
if($("#add_candidates").valid()){


   document.getElementById('resQuotes').style.display = "block";
    
   $(window).scrollTop($("*:contains('PHP'):last").offset().top);
    
    var  ownDamage = document.getElementById('cost').value ;  
    var  name = document.getElementById('name').value ;  
    var  contact = document.getElementById('contact').value ;  
    var  city = document.getElementById('city').value ;  
    var  email = document.getElementById('email').value ; 
     var  bike = document.getElementById('bike').value ; 
     var  yearbike = document.getElementById('yearbike').value ; 
    //var  typeM = document.getElementById('typeM').value ; 
     var  cost = document.getElementById('cost').value ; 
      var  cmt = document.getElementById('cmt').value ; 
    //var  engine = document.getElementById('engine').value ; 
     //var  plate = document.getElementById('plate').value ;
      //var radios = document.getElementsByName('optradio').value ;
       var mort = document.querySelector('input[name=optradio]:checked').value;
  //     var mort ="";
  //     for (var i = 0, length = radios.length; i < length; i++)
  //        {
  //           if (radios[i].checked)
  //         {
  // // do whatever you want with the checked radio
  //        mort =radios[i].value;
  // // only one radio can be logically checked, don't check the rest
  //          break;
  //         }
  //       }

   document.getElementById('ownDamage').value= ownDamage;
   var premiumOwnDamge= ownDamage *2.5/100;
   var actOfGod = ownDamage ;
    var actOfGodPremium = "INCLUSIVE" ;
    var bodyInjury = 150000;
    var bodyInjuryPremium = 105;   //Property Damage
    var PropertyDamage =100000;
    var PropertyDamage_premium =555;
   // auto seat
   var autoSeat = 100000;
   var autoSeatPremium = "FREE";
   

   var basicPremium = premiumOwnDamge + bodyInjuryPremium + PropertyDamage_premium ;
   var ValueAddedTax = basicPremium * 12/100;
   var DomentaryStamp = basicPremium * 12.5/100;
   var localGovernment = basicPremium * 0.5/100;
   var taxes =0;
  var  otherCharges = 0;
  var total_Premium = (basicPremium + taxes + DomentaryStamp +ValueAddedTax+localGovernment+otherCharges);
   document.getElementById('taxes').value= taxes;
   document.getElementById('DomentaryStamp').value= DomentaryStamp.toFixed(2);
   document.getElementById('ValueAddedTax').value= ValueAddedTax.toFixed(2); 
   document.getElementById('localGovernment').value= localGovernment.toFixed(2); 
    document.getElementById('otherCharges').value= otherCharges.toFixed(2); 

   
   //lable elements 
     document.getElementById('premium').innerHTML= basicPremium ; 
     document.getElementById('tax').innerHTML= "" ; 
     document.getElementById('stamp').innerHTML= DomentaryStamp.toFixed(2) ; 
   document.getElementById('value').innerHTML= ValueAddedTax.toFixed(2) ; 
   document.getElementById('local').innerHTML=  localGovernment.toFixed(2);
   document.getElementById('other').innerHTML=  otherCharges;
    document.getElementById('total_premium').innerHTML= "PHP"+" "+total_Premium.toFixed(2)  ;
   
    
     document.getElementById('theft').innerHTML= "PHP"+" "+cost; 
     document.getElementById('damage').innerHTML="PHP"+" "+ cost; 
     document.getElementById('injury').innerHTML= "PHP"+" "+bodyInjury ; 
   document.getElementById('accident').innerHTML= "PHP"+" "+bodyInjury.toFixed(2) ; 
   document.getElementById('act').innerHTML=  "PHP"+" "+actOfGod;
   

     document.getElementById('actOfGod').value= actOfGod; 
     document.getElementById('PropertyDamage').value= PropertyDamage; 
     document.getElementById('autoSeat').value= autoSeat; 
     document.getElementById('bodyInjury').value= bodyInjury; 
  // $premiumOwnDamge = $ownDamage *2.5/100;
      document.getElementById('total_pre').value= total_Premium.toFixed(2); 
     // document.getElementById('engineN').value= engine; 
    // document.getElementById('plateN').value= plate; 
     

     document.getElementById('namee').value = name;
     document.getElementById('contactt').value = contact;
     document.getElementById('cityy').value  = city;
     document.getElementById('emaill').value = email;
     document.getElementById('bikee').value = bike;
     document.getElementById('yearbikee').value = yearbike;
     //document.getElementById('typeMM').value = typeM;
     document.getElementById('costt').value = cost;
      document.getElementById('Mortgagee').value= mort; 
       document.getElementById('cmtt').value= cmt; 

	// $.blockUI({ css: { 
 //            border: 'none', 
 //            padding: '15px',
 //            backgroundColor: '#000', 
 //            '-webkit-border-radius': '10px', 
 //            '-moz-border-radius': '10px', 
 //            opacity: .5, 
 //            color: '#fff' 
 //        } }); 


 //    var formData = new FormData($('#add_candidates')[0]);
 //    var ajaxRequest = $.ajax({
 //        type: "POST",
 //        url: 'inc/managepost.php',       
 //        contentType: false,
 //        processData: false,
 //        data:formData

 //    });
 //    ajaxRequest.done(function (data,xhr, textStatus) {
 //    	$.unblockUI();
    	
 //           // $('#items_container').append(json[i].item_id)
            
 //     //  var myResult = data.responseText;
 //         console.log(data);
 //       // if(xhr==true) {
 //           // window.location.href = "bookinsurance.php"
 //           //$('#resQuotes').show();
 //        	  document.getElementById('resQuotes').style.display = 'block'; 
 //              document.getElementById('sub_Total').value= data.taxe
 //        	 // var taxes = document.getElementById(data.taxes);
        	 //   taxes=xhr.taxes;s
        	//$("##debitoptions").html($(data).find('#flash-msg'))
          

          //toastr.success("New Partner Has Been Added Successfully", "Status");
           //$('#add_partner')[0].reset();
        // }else {
        //    // toastr.error("Sorry An Error Occured Please Check and Try Again", "Status")
           
       // }

   // });

  }
 });

$('#submit_email').submit(function(e){
e.preventDefault();
console.log($("#submit_email").valid());
if($("#submit_email").valid()){
$("#submit_email").val();
	$.blockUI({ css: { 
            border: 'none', 
            padding: '15px',
            backgroundColor: '#000', 
            '-webkit-border-radius': '10px', 
            '-moz-border-radius': '10px', 
            opacity: .5, 
            color: '#fff' 
        } }); 


    var formData = new FormData($('#submit_email')[0]);
    var ajaxRequest = $.ajax({
        type: "POST",
        url: 'inc/managepost.php',
        contentType: false,
        processData: false,
        data:formData

    });
    ajaxRequest.done(function (xhr, textStatus) {
    	$.unblockUI();
        console.log(xhr);
        if(xhr==1) {
           alert("Thank you for using book2wheel.com motorbike insurance service. Now the insurance company will " +
           " contact you through email and send you complete details of comprehensive insurance. The agent will "+
             " contact your during office hour between Monday to Friday. You can ask more questions to the insurance"+
             " agent through email");
            window.location.reload();
           // document.getElementById('resQuotes').style.display = "none";
           // document.getElementById('res').style.display = "none";
           // document.getElementById('resShow').style.display = "block";
           
          //toastr.success("Email has been sent succesffully.", "Status");
           //$('#add_insurance')[0].reset();
        }else {
           // toastr.error("Sorry An Error Occured Please Check and Try Again", "Status")
           
        }

    });

  }
 });